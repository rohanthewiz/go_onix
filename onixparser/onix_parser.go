package onixparser
import (
	"encoding/xml"
	"github.com/rethinkbooks/go-onix/onixmessage"
	"github.com/rethinkbooks/go-onix/onixmessage2_1"
	"github.com/rethinkbooks/go-onix/onixmessage3"
	"github.com/rethinkbooks/go-onix/onixnormalized"
	"fmt"
	"io"
	"strings"
)

type OnixParser struct {
	decoder *xml.Decoder
	onix_message onixmessage.OnixMessage
	onix_normalized *onixnormalized.OnixNormalized
	onix_release string
}

func NewOnixParser(xmlFile io.Reader) *OnixParser {
	op := new(OnixParser)
	op.decoder = xml.NewDecoder(xmlFile) // save pointer to the decoder
	op.onix_normalized = onixnormalized.NewOnixNormalized() // Output struct
	return op
}

func (op *OnixParser) Parse() {
	for {
		// Read tokens from the XML document in a stream.
		token, err := op.decoder.Token() // get a token
		if token == nil || err != nil {
			break
		}
		// Start parsing just to determine Onix Msg type (Release)
		switch se := token.(type) {  // get the concrete type out of the token interface.

		case xml.Directive:
			fmt.Printf("Directive: %s\n", string(se))

		case xml.StartElement:  // If token is a StartElement
			if strings.ToLower(se.Name.Local) == "onixmessage" {
				// Todo normalize case on *all* comparisons
				fmt.Println("Found ONIXMessage")
				for _, attr := range se.Attr {
					if strings.ToLower(attr.Name.Local) == "release" {
						op.onix_release = attr.Value
						fmt.Println("Release:", attr.Value)
					}
				}
				if op.onix_release == "3" {
					op.onix_message = onixmessage3.NewOnixMessage3(op.onix_normalized)
				} else {
					op.onix_message = onixmessage2_1.NewOnixMessage2_1(op.onix_normalized)
				}
				// Now tell the Onix Message to continue parsing Header and Products
				// Note that a pointer is passed here (op.decoder) so we are referring to the same decoder object
				op.onix_message.ParseOnix(op.decoder, op.onix_release)
				return
			}
		}
	}
}

// Marshal out the specific type of Onix Message
func (op *OnixParser) MarshalXML() {
	fmt.Println(op.onix_message.Marshal())
}

// Marshal out the Normalized data
func (op *OnixParser) MarshalNormalizedJSON() string {
	return op.onix_normalized.ToJSON()
}

func (op *OnixParser) PrintNormalized() {
	fmt.Println("Normalized Onix data")
	for _, prod := range op.onix_normalized.Products {
		fmt.Printf("Product -> RecordRef: %s, ISBN: %s, Title: %s\n", prod.RecordReference, prod.ISBN, prod.Title)
	}
}
