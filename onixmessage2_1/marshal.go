package onixmessage2_1

import (
	"github.com/rethinkbooks/go-onix/onixnormalized"
	"encoding/xml"
)

func (om OnixMessage2_1) Marshal() string {

	var outstr string
	for _, prod := range om.onix_normalized.Products {
		outstr += marshalProduct(prod) + "\n"
	}
	return outstr
}

func marshalProduct(np onixnormalized.Product) string {
	str, err := xml.MarshalIndent(np, " ", "  ")
	if err != nil {
		return ""
	}
	return string(str)
}
