package onixmessage2_1

import "github.com/rethinkbooks/go-onix/onixnormalized"

// Constructor
func NewOnixMessage2_1(normalized *onixnormalized.OnixNormalized) *OnixMessage2_1 {
	om := new(OnixMessage2_1)
	om.onix_normalized = normalized
	return om
}
