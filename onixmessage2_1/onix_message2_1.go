package onixmessage2_1
import "github.com/rethinkbooks/go-onix/onixmessage"
import "github.com/rethinkbooks/go-onix/onixnormalized"

type OnixMessage2_1 struct {
	xmlStr         []byte
	Release string `xml:"release,attr"`
	Header Header `xml:"header"`
	Products []OnixProduct `xml:"Product"`
	onix_normalized *onixnormalized.OnixNormalized
}

type Header struct {
	SenderIdentifier SenderIdentifier `xml:"SenderIdentifier"`
	FromCompany onixmessage.OnixLeafNode `xml:"FromCompany"`
	SentDate onixmessage.OnixLeafNode `xml:"SentDate"`
	FromEmail onixmessage.OnixLeafNode `xml:"FromEmail"`
}

type SenderIdentifier struct {
	SenderIDType onixmessage.OnixLeafNode `xml:"SenderIDType"`
	IDValue onixmessage.OnixLeafNode `xml:"IDValue"`
}

type OnixProduct struct {
	RecordReference onixmessage.OnixLeafNode `xml:"RecordReference"`
	NotificationType string
	ProductIdentifiers []ProductIdentifier `xml:"ProductIdentifier"`
	EpubType string
	Title Title `xml:"Title"`
	Publisher Publisher `xml:"Publisher"`
	SupplyDetail ChiSupplyDetail `xml:"SupplyDetail"`
}

type ChiSupplyDetail struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	ChiPrice []*ChiPrice `xml:" Price,omitempty" json:"Price,omitempty"`
}

type ChiPrice struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	ChiCountryCode []*ChiCountryCode `xml:" CountryCode,omitempty" json:"CountryCode,omitempty"`
	ChiCurrencyCode *ChiCurrencyCode `xml:" CurrencyCode,omitempty" json:"CurrencyCode,omitempty"`
	ChiPriceAmount *ChiPriceAmount `xml:" PriceAmount,omitempty" json:"PriceAmount,omitempty"`
	ChiPriceEffectiveFrom *ChiPriceEffectiveFrom `xml:" PriceEffectiveFrom,omitempty" json:"PriceEffectiveFrom,omitempty"`
	ChiPriceTypeCode *ChiPriceTypeCode `xml:" PriceTypeCode,omitempty" json:"PriceTypeCode,omitempty"`
}

type ChiPriceEffectiveFrom struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	Text string `xml:",chardata" json:",omitempty"`
}
type ChiPriceTypeCode struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	Text string `xml:",chardata" json:",omitempty"`
}

type ChiPriceAmount struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	Text string `xml:",chardata" json:",omitempty"`
}

type ChiCurrencyCode struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	Text string `xml:",chardata" json:",omitempty"`
}

type ChiCountryCode struct {
	Attr_refname string `xml:" refname,attr"  json:",omitempty"`
	Attr_shortname string `xml:" shortname,attr"  json:",omitempty"`
	Text string `xml:",chardata" json:",omitempty"`
}
type Title struct {
	TitleType onixmessage.OnixLeafNode `xml:"TitleType"`
	TitleText onixmessage.OnixLeafNode `xml:"TitleText"`
}

type ProductIdentifier struct {
	ProductIDType onixmessage.OnixLeafNode `xml:"ProductIDType"`
	IDTypeName onixmessage.OnixLeafNode `xml:"IDTypeName"`
	IDValue onixmessage.OnixLeafNode `xml:"IDValue"`
}

type Publisher struct {
	PublisherName onixmessage.OnixLeafNode `xml:"PublisherName"`
	PublisherWebsite Website `xml:"Website"`
}

type Website struct {
	WebsiteLink onixmessage.OnixLeafNode `xml:"WebsiteLink"`
}