package onixmessage2_1
import (
	"github.com/rethinkbooks/go-onix/onixnormalized"
)

func (om *OnixMessage2_1) buildNormalizedHeader() onixnormalized.Header {
	var header onixnormalized.Header
	header.FromCompany = om.Header.FromCompany.Value
	header.SenderIdentifier = om.Header.SenderIdentifier.IDValue.Value
	header.SentDate = om.Header.SentDate.Value
	header.Email = om.Header.FromEmail.Value
	return header
}

func (om *OnixMessage2_1) buildNormalizedProduct(product OnixProduct) onixnormalized.Product {
	var np onixnormalized.Product // Normalized Product Output

	// RecordRef
	np.RecordReference = product.RecordReference.Value

	// ISBN
	for _, pi := range product.ProductIdentifiers {
		if pi.ProductIDType.Value == "15" {
			np.ISBN = pi.IDValue.Value
		} else if pi.ProductIDType.Value == "03" {
			np.ISBN = pi.IDValue.Value
		}
	}

	np.Title = product.Title.TitleText.Value

	np.Publisher = product.Publisher.PublisherName.Value +
		"(" + product.Publisher.PublisherWebsite.WebsiteLink.Value + ")"

	// Prices
	for _, price := range product.SupplyDetail.ChiPrice {
		var norm_price onixnormalized.Price
		norm_price.PriceAmount = price.ChiPriceAmount.Text
		norm_price.TypeCode = price.ChiPriceTypeCode.Text
		np.Prices = append(np.Prices, norm_price)
	}

	return np
}
