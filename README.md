This is a work-in-progress on parsing onix files in Go. See Rohan (ro | robks) with any questions.

### Setup

#### Go Setup
* Download and install Go: https://golang.org/dl/
* Create a folder off of your $HOME that will be your Go workspace. Example:

```
mkdir ~/go
```

* In your .profile | .bashrc | .zshrc 
 add an environment variable `GOPATH` to point to your Go workspace. Example:

```
export GOPATH=$HOME/go
```

### Get the app
Once you have the `go` executable installed and `GOPATH` set to your go workspace, you can pull down the app

```
go get -u github.com/rethinkbooks.com/go-onix...
```

### Configuring The App

#### Create your options.yml file

1. Change to project root `cd $GOPATH/src/github.com/rethinkbooks.com/go-onix`
2. Copy options from sample `cp config/options.yml.sample config/options.yml`

#### How to Setup your AWS Profile
Profiles is a way of managing multiple AWS accounts

File: `~/.aws/credentials`

```
[default]
aws_access_key_id=YOUR_ACCESS_KEY
aws_secret_access_key=YOUR_SECRET_KEY

[bookshout]
aws_access_key_id=BOOKSHOUT_ACCESS_KEY
aws_secret_access_key=BOOKSHOUT_SECRET_KEY
```

### Building The Project
```
cd $GOPATH/src/github.com/rethinkbooks.com/go-onix
go build  # this will produce the executable `go-onix` in the current directory
```

### Running The Project

```
./go-onix
```
