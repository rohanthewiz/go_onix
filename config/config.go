package config
import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"fmt"
)

const config_file = "config/options.yml"

type Config struct {
	AWS struct {
			Profile string `yaml:"profile"`
			Region string `yaml:"region"`
		S3 struct {
			Bucket string `yaml:"bucket"`
			DoUpload bool `yaml:"doUpload"`
		}
	}
	TestFile string `yaml:"test_file"`
}

var Options *Config  // our app configuration

func InitConfig() {
	Options = new(Config)
	loadConfig()
	//fmt.Printf("Our config options are:%#v\n", Options.AWS)

	if Options.AWS.Profile != "" {
		os.Setenv("AWS_PROFILE", Options.AWS.Profile)  // tell AWS to use this profile
	}
	if Options.AWS.Region != "" {
		os.Setenv("AWS_REGION", Options.AWS.Region)  // tell AWS to use this profile
	}
}

func loadConfig() error {
	options_data, err := ioutil.ReadFile(config_file)
	if err != nil {
		fmt.Println("Hey did you remember to copy 'config/options.yml.sample' to 'config/options.yml' ?")
		return err
	}
	// Read the data into our options struct
	err = yaml.Unmarshal(options_data, Options)
	return err
}
