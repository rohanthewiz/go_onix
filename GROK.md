# Detailed Explanations and Tips

### GO TIPS
Generally your Go projects should live under a folder path of this format:

```
$GOPATH/src/yourdomain.com/your_username/your_project
```

Any Github dependencies you pull in will go under:

```
$GOPATH/src/github.com/some_dude/some_dudes_project
```

You can check your current GOPATH and other Go related environment variables by running the following command
```
go env
```

Curious about the other folders under $GOPATH?

- **pkg** is where dependencies' linkable object files kept
- **bin** is where executables are installed to if you run the 'go install' command
 