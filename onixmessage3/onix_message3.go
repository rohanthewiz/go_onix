package onixmessage3
import "github.com/rethinkbooks/go-onix/onixmessage"
import "github.com/rethinkbooks/go-onix/onixnormalized"

type OnixMessage3 struct {
	xmlStr         []byte
	Release string `xml:"release,attr"`
	Header Header `xml:"header"`
	Products []OnixProduct `xml:"Product"`
	onix_normalized *onixnormalized.OnixNormalized
}

type Header struct {
	SenderIdentifier SenderIdentifier `xml:"SenderIdentifier"`
	FromCompany onixmessage.OnixLeafNode `xml:"FromCompany"`
	SentDate onixmessage.OnixLeafNode `xml:"SentDate"`
	FromEmail onixmessage.OnixLeafNode `xml:"FromEmail"`
}

type SenderIdentifier struct {
	SenderIDType onixmessage.OnixLeafNode `xml:"SenderIDType"`
	IDValue onixmessage.OnixLeafNode `xml:"IDValue"`
}

type OnixProduct struct {
	RecordReference onixmessage.OnixLeafNode `xml:"RecordReference"`
	NotificationType string
	ProductIdentifiers []ProductIdentifier `xml:"ProductIdentifier"`
	EpubType string
	Title Title `xml:"Title"`
	Publisher Publisher `xml:"Publisher"`
}

type Title struct {
	TitleType onixmessage.OnixLeafNode `xml:"TitleType"`
	TitleText onixmessage.OnixLeafNode `xml:"TitleText"`
}

type ProductIdentifier struct {
	ProductIDType onixmessage.OnixLeafNode `xml:"ProductIDType"`
	IDTypeName onixmessage.OnixLeafNode `xml:"IDTypeName"`
	IDValue onixmessage.OnixLeafNode `xml:"IDValue"`
}

type Publisher struct {
	PublisherName onixmessage.OnixLeafNode `xml:"PublisherName"`
	PublisherWebsite Website `xml:"Website"`
}

type Website struct {
	WebsiteLink onixmessage.OnixLeafNode `xml:"WebsiteLink"`
}