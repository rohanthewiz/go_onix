package onixmessage3

import (
	"github.com/rethinkbooks/go-onix/onixnormalized"
	"encoding/xml"
)

// This is just a demo that we can return our normalized data however we want
func (om OnixMessage3) Marshal() string {
	var outstr string
	for _, prod := range om.onix_normalized.Products {
		outstr += marshalProduct(prod) + "\n"
	}
	return outstr
}

func marshalProduct(np onixnormalized.Product) string {
	str, err := xml.MarshalIndent(np, " ", "  ")
	if err != nil {
		return ""
	}
	return string(str)
}
