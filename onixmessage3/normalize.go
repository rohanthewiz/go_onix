package onixmessage3
import (
	"github.com/rethinkbooks/go-onix/onixnormalized"
)

func (om *OnixMessage3) buildNormalizedHeader() onixnormalized.Header {
	var header onixnormalized.Header
	header.FromCompany = om.Header.FromCompany.Value
	header.SenderIdentifier = om.Header.SenderIdentifier.IDValue.Value
	header.SentDate = om.Header.SentDate.Value
	header.Email = om.Header.FromEmail.Value
	return header
}

func (om *OnixMessage3) buildNormalizedProduct(product OnixProduct) onixnormalized.Product {
	var np onixnormalized.Product
	// RecordRef
	np.RecordReference = product.RecordReference.Value
	// ISBN
	for _, pi := range product.ProductIdentifiers {
		if pi.ProductIDType.Value == "03" {
			np.ISBN = pi.IDValue.Value
		}
	}
	np.Title = product.Title.TitleText.Value
	np.Publisher = product.Publisher.PublisherName.Value +
		"(" + product.Publisher.PublisherWebsite.WebsiteLink.Value + ")"
	return np
}
