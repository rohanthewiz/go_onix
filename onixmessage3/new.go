package onixmessage3

import "github.com/rethinkbooks/go-onix/onixnormalized"

// Constructor
func NewOnixMessage3(normalized *onixnormalized.OnixNormalized) *OnixMessage3 {
	om := new(OnixMessage3)
	om.onix_normalized = normalized
	return om
}
