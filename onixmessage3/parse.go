package onixmessage3

import (
	"encoding/xml"
	"sync"
	"fmt"
	"strings"
)
const products_limit  = 1000

var mutex = new(sync.Mutex)
var wait_group = new(sync.WaitGroup)

func (om *OnixMessage3) ParseOnix(decoder *xml.Decoder, release string) {
	fmt.Println("Parsing OnixMessage3...")
	om.Release = release
	om.onix_normalized.Release = release

	om.parseStream(decoder)
	// demo: om.PrintRaw()
}

func (om *OnixMessage3) parseStream(decoder *xml.Decoder) {
	var product_counter uint64 // vars are automatically initialized to their zero value

	for {
		// Read tokens from the XML document in a stream.
		token, err := decoder.Token() // get a token
		if token == nil || err != nil {
			break
		}
		switch se := token.(type) {  // get the concrete type out of the token interface.
		case xml.StartElement:  // If token is a StartElement
			// Header
			if strings.ToLower(se.Name.Local) == "header" {
				decoder.DecodeElement(&om.Header, &se)  // decode into our message Header
				//fmt.Printf("Parsed Header ->%#v\n", om.Header)
				wait_group.Add(1)  // track goroutine
				go om.normalizeHeader()  // launch a goroutine
			}
			// Product
			if strings.ToLower(se.Name.Local) == "product" {
				var prod OnixProduct
				decoder.DecodeElement(&prod, &se)
				//fmt.Printf("Parsed Product ->%#v\n", prod)
				product_counter += 1
				wait_group.Add(1)  // track goroutine
				go om.addProduct(prod, product_counter) // do this concurrently
			}
		}
		// Truncate the list
		if product_counter >= products_limit {
			fmt.Println("Output truncated to", products_limit)
			break
		}
	}
	wait_group.Wait()  // wait for all goroutines to finish
	fmt.Println("\n", product_counter, "Products found")
}

func (om *OnixMessage3) normalizeHeader() {
	om.onix_normalized.AddHeader(om.buildNormalizedHeader())
	wait_group.Done()  // this goroutine is finished
}

func (om *OnixMessage3) addProduct(prod OnixProduct, index uint64) {
	fmt.Print(".")
	// Add Normalized product
	om.onix_normalized.AddProduct(om.buildNormalizedProduct(prod), index)
	wait_group.Done()  // this goroutine is finished
}
