package aws

import (
	"github.com/aws/aws-sdk-go/service/s3"
	aws_sdk "github.com/aws/aws-sdk-go/aws"
	"log"
	"strings"
)

var s3_client *s3.S3  // cache the client

func S3InitClient() error {
	var err error
	if s3_client != nil {
		return  nil // the client is already inited
	}
	if aws_session == nil {
		err = InitSession()
		if err != nil {
			return err
		}
	}
	s3_client = s3.New(aws_session)
	return err
}

func S3ListBuckets() {
	err := S3InitClient()
	if err != nil {
		log.Println("Error initializing S3 Client")
	}

	result, err := s3_client.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		log.Println("Failed to list buckets", err)
	}
	log.Println("Buckets:")
	for _, bucket := range result.Buckets {
		log.Printf("%s : %s", aws_sdk.StringValue(bucket.Name), bucket.CreationDate)
	}

}

func S3CreateBucket(bucket_name string) error {
	err := S3InitClient()
	if err != nil {
		log.Println("Error initializing S3 Client")
	}
	bucket := bucket_name
	_, err = s3_client.CreateBucket(&s3.CreateBucketInput{
		Bucket: aws_sdk.String(bucket),
	})
	if err != nil {
		log.Println("Failed to create bucket", err)
		return err
	}

	if err = s3_client.WaitUntilBucketExists(&s3.HeadBucketInput{Bucket: &bucket_name}); err != nil {
		log.Printf("Failed to wait for bucket to exist %s, %s\n", bucket_name, err)
		return err
	}
	return err
}

func S3UploadFile(key string, bucket string, data string) error {
	err := S3InitClient()
	if err != nil {
		log.Println("Error initializing S3 Client")
		return err
	}
	_, err = s3_client.PutObject(&s3.PutObjectInput{
		Body: strings.NewReader(data),
		Bucket: aws_sdk.String(bucket),
		Key: aws_sdk.String(key),
	})
	if err != nil {
		log.Println("Error uploading to S3")
	}
	return err
}
