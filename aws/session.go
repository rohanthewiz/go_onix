package aws

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"fmt"
	aws_sdk "github.com/aws/aws-sdk-go/aws"
	"github.com/rethinkbooks/go-onix/config"
)

// Cache Session
var aws_session *session.Session

func InitSession() error {
	sess, err := session.NewSession(&aws_sdk.Config{
		Region: aws_sdk.String(config.Options.AWS.Region),
	})
	if err != nil {
		fmt.Println("Error creating AWS session:", err)
		return err
	}
	aws_session = sess
	return err
}
