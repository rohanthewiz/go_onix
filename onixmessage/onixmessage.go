// A Few common low-level things
package onixmessage

import "encoding/xml"

// Avoid any imports here
//import "github.com/rethinkbooks/go-onix/onixnormalized"

// An Onix Message type should provide a ParseOnix function
// Note: Any struct containing the listed methods automatically satisfies this interface
type OnixMessage interface {
	ParseOnix(decoder *xml.Decoder, release string)
	Marshal() string
}

type OnixLeafNode struct {
	ShortName string `xml:"shortname,attr"`
	Value     string `xml:",chardata"`
}

// Private

// Generic message struct for determining message type
type OnixGeneric struct {
	Release string `xml:"release,attr"`
}
