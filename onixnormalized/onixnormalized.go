package onixnormalized

import (
	"sync"
	"encoding/json"
	"github.com/rethinkbooks/go-onix/aws"
	"fmt"
	"github.com/rethinkbooks/go-onix/config"
)

var mutex = new(sync.Mutex)

type OnixNormalized struct {
	Release string
	Header Header
	Products []Product `json:"products"`
}

type Header struct {
	FromCompany string
	SenderIdentifier string
	SentDate string
	Email string
}

type Product struct {
	RecordReference string `json:"recordreference"`
	ISBN string `json:"isbn"`
	Title string
	Publisher string
	Prices []Price `json:"prices"`
	//LastUpdate string // keep as a string here for now
}

type Price struct {
	TypeCode string
	PriceAmount string
}

func NewOnixNormalized() *OnixNormalized {
	return new(OnixNormalized)
}

// This can be called concurrently (locks needed!)
func (on *OnixNormalized) AddHeader(hdr Header) {
	mutex.Lock()  // allow only one goroutine to access
	on.Header = hdr
	mutex.Unlock()
}

// This can be called concurrently (locks needed!)
func (on *OnixNormalized) AddProduct(prod Product, index uint64) {
	mutex.Lock() // for concurrency - lock the data
	on.Products = append(on.Products, prod)
	mutex.Unlock()
	json_prod := on.ToJsonProduct(prod)
	if config.Options.AWS.S3.DoUpload {
		err := on.UploadJSON(fmt.Sprintf("product-%d.json", index + 1), json_prod)
		if err != nil {
			fmt.Println("Product", index + 1, "failed to upload")
		}
	}
}

func (on *OnixNormalized) ToJSON() string {
	str, err := json.MarshalIndent(on, " ", "  ")
	if err != nil {
		return ""
	}
	return string(str)
}

func (on *OnixNormalized) ToJsonProduct(prod Product) string {
	str, err := json.MarshalIndent(&prod, "", "  ")
	if err != nil {
		return ""
	}
	return string(str)
}

func (on *OnixNormalized) UploadJSON(name string, data string) error {
	// todo //if config.UploadEnabled { }
	return aws.S3UploadFile(name, config.Options.AWS.S3.Bucket, data)
}
