package main
import (
	"os"
	"log"
	"fmt"
	"runtime"
	"github.com/rethinkbooks/go-onix/onixparser"
	"github.com/rethinkbooks/go-onix/config"
	"github.com/rethinkbooks/go-onix/aws"
)

func main() {
	// Load our options
	config.InitConfig()

	// Configure multi-core
	if os.Getenv("GOMAXPROCS") == "" {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	//aws.S3CreateBucket(config.Options.AWS.S3.Bucket)
	// List Buckets
	aws.S3ListBuckets()

	// Open XML file
	xmlFile, err := os.Open(config.Options.TestFile)
	if err != nil {
		log.Fatal("Cannot open input file:", config.Options.TestFile)
	}
	defer xmlFile.Close()  // close the file when the function exits at any point - nice

	// Process
	onix_parser := onixparser.NewOnixParser(xmlFile)
	onix_parser.Parse()
	// dev only: onix_parser.PrintNormalized()
	out_str := onix_parser.MarshalNormalizedJSON()
	fmt.Println(out_str)

	// Uploading individual JSON files instead //Upload to AWS
	//if (len(out_str) > 0) {
	//	aws.S3UploadFile("onix3.json", "bookshout.onix", out_str)
	//}
}
